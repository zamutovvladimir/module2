﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {            
            Program module = new Program();

            // Проверка первого задания.
            Console.WriteLine("GetTotalTax\t{0}", module.GetTotalTax(53, 50, 10));

            // Проверка второго задания.
            Console.Write("Введите возраст (положительное целочисленное значение): ");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("GetCongratulation\t{0}", module.GetCongratulation(age));

            // Проверка третьего задания.
            Console.WriteLine("GetMultipliedNumbers\t{0}", module.GetMultipliedNumbers("123,456", "1000"));
            Console.WriteLine("GetMultipliedNumbers\t{0}", module.GetMultipliedNumbers("123.456", "1000"));

            // Проверка четвертого задания.
            Dimensions dim = new Dimensions();
            dim.FirstSide = 38;
            dim.SecondSide = 36;
            dim.ThirdSide = 17;
            dim.Height = 0;
            dim.Diameter = 50;
            dim.Radius = 25;

            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Rectangle, ParameterEnum.Perimeter, dim));
            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Rectangle, ParameterEnum.Square, dim));
            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Perimeter, dim));
            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Triangle, ParameterEnum.Square, dim));
            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Circle, ParameterEnum.Perimeter, dim));
            Console.WriteLine("GetFigureValues\t{0}",
                module.GetFigureValues(FigureEnum.Circle, ParameterEnum.Square, dim));

            // Отображение результата в консоли до нажатия клавиши Enter.
            Console.ReadLine();
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return ((companiesNumber * companyRevenue) * tax/100);
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >=18 )
            {
                return "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                return "Поздравляю с переходом в старшую школу!";
            }
            return String.Format("Поздравляю с {0}-летием!", input);
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-EN");

            double a, b;
            first = first.Replace(",", ".");
            second = second.Replace(",", ".");
           
            if (!double.TryParse(first, out a))
            { 
                throw new ArgumentException("Некоректный ввод первого числа!");
            }

            if (!double.TryParse(second, out b))
            {
                throw new ArgumentException("Некоректный ввод второго числа!");
            }

            return (a*b);
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            double result = 0;
            switch (figureType)
            {
                case FigureEnum.Triangle:
                    if (parameterToCompute == ParameterEnum.Perimeter)
                    {
                        result = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    }
                    else
                    {
                        if (dimensions.Height != 0)
                        {
                           result = 0.5 * dimensions.FirstSide * dimensions.Height;
                        }
                        else
                        {
                            double sp = 0.5 * (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
                            result = Math.Sqrt(sp * (sp - dimensions.FirstSide) * (sp - dimensions.SecondSide) * (sp - dimensions.ThirdSide));
                        }
                        result = Math.Truncate(result);
                    }
                    
                    break;
                case FigureEnum.Rectangle:
                    if (parameterToCompute == ParameterEnum.Perimeter)
                    {
                        result = (dimensions.FirstSide + dimensions.SecondSide) * 2;
                    }
                    else result = dimensions.FirstSide * dimensions.SecondSide;
                    break;
                case FigureEnum.Circle:
                    if (parameterToCompute == ParameterEnum.Perimeter)
                    {
                        result = 2*Math.PI*dimensions.Radius;
                    }
                    else result = Math.PI*(Math.Pow(dimensions.Radius,2));
                    Math.Truncate(result);
                    break;
            }
            return Math.Round(result);
        }
    }
}